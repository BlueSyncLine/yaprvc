module core(
    input wire clk,
    input wire hold,
    input wire reset,

    output wire[29:0] inst_word_addr,
    input wire[31:0] inst_word,
    output wire[29:0] data_word_addr,
    input wire[31:0] data_word_read,
    output wire[31:0] data_word_write,
    output wire data_read,
    output wire data_write,
    output wire[3:0] data_write_mask,

    output wire breakpoint
);

reg[4:0] decode_rs1, decode_rs2, writeback_rd;
wire[31:0] regs_rs1_val, regs_rs2_val, decode_inst, decode_branch_expect_pc,
    execute_inst, access_inst, execute_branch_refetch_addr, execute_write_val, execute_mem_addr,
    access_reg_wb, writeback_data;
wire[29:0] decode_inst_word_addr;
wire[1:0] access_offset;
wire decode_bubble, decode_branch_expect, execute_branch_refetch, fetch_alignment_error, access_alignment_error,
    execute_invalid_op, execute_ebreak;
wire[1:0] decode_interdepend;

regfile regs(
    .clk(clk),
    .hold(hold),
    .rs1(decode_rs1),
    .rs2(decode_rs2),
    .read_data_1(regs_rs1_val),
    .read_data_2(regs_rs2_val),
    .rd(writeback_rd),
    .write_data(writeback_data)
);

fetcher fetch(
    .clk(clk),
    .hold(hold),
    .reset(reset),

    .branch_expect(decode_branch_expect),
    .branch_expect_pc(decode_branch_expect_pc),
    .branch_refetch(execute_branch_refetch),
    .branch_refetch_addr(execute_branch_refetch_addr),

    .bubble(decode_bubble),

    .fetch_word_addr(inst_word_addr),
    .inst_alignment_error(fetch_alignment_error)
);

decoder decode(
    .clk(clk),
    .hold(hold),
    .reset(reset),

    .branch_expect(decode_branch_expect),
    .branch_expect_pc(decode_branch_expect_pc),
    .branch_refetch(execute_branch_refetch),

    .rs1(decode_rs1),
    .rs2(decode_rs2),

    .inst(inst_word),
    .inst_word_addr(inst_word_addr),
    .inst_out(decode_inst),
    .inst_word_addr_out(decode_inst_word_addr),
    .mem_inst(execute_inst),
    .bubble(decode_bubble),
    .interdepend_exec(decode_interdepend)
);

executor execute(
    .clk(clk),
    .hold(hold),
    .reset(reset),

    .inst(decode_inst),
    .inst_out(execute_inst),
    .inst_word_addr(decode_inst_word_addr),
    .rs1_val(regs_rs1_val),
    .rs2_val(regs_rs2_val),
    .interdepend(decode_interdepend),
    .branch_refetch(execute_branch_refetch),
    .branch_refetch_addr(execute_branch_refetch_addr),
    .write_val(execute_write_val),
    .mem_addr(execute_mem_addr),
    .invalid_op(execute_invalid_op),
    .ebreak(execute_ebreak)
);

mem_acc access(
    .clk(clk),
    .hold(hold),
    .reset(reset),

    .inst(execute_inst),
    .mem_addr(execute_mem_addr),
    .write_val(execute_write_val),
    .inst_out(access_inst),
    .reg_wb_out(access_reg_wb),
    .read(data_read),
    .write(data_write),
    .access_word_addr(data_word_addr),
    .access_offset(access_offset),
    .access_write_mask(data_write_mask),
    .access_write_data(data_word_write),
    .alignment_error(access_alignment_error)
);

wb writeback(
    .hold(hold),
    .reset(reset),
    .inst(access_inst),
    .rd(writeback_rd),
    .offset(access_offset),
    .data_in_mem(data_word_read),
    .data_in_reg(access_reg_wb),
    .data_out(writeback_data)
);

assign breakpoint = fetch_alignment_error || access_alignment_error || execute_invalid_op ||
        execute_ebreak;
endmodule
