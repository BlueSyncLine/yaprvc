`include "inst.v"
`include "opcodes.v"
`include "interdepend.v"
`include "alu.v"

module executor(
    input wire clk,
    input wire hold,
    input wire reset,

    input wire[31:0] inst,
    input wire[29:0] inst_word_addr,
    input wire[31:0] rs1_val,
    input wire[31:0] rs2_val,
    input wire[1:0] interdepend,
    output reg[31:0] inst_out,
    output reg[31:0] write_val,
    output reg[31:0] mem_addr,
    output reg branch_refetch,
    output reg[31:0] branch_refetch_addr,
    output reg invalid_op,
    output wire ebreak
);

wire alu_uses_immediate = inst_opcode(inst) == `OPCODE_OP_IMM;
wire[31:0] alu_x = interdepend[0] ? write_val : rs1_val;
wire[31:0] alu_y = alu_uses_immediate ? inst_Iimm(inst) :
                    interdepend[1] ? write_val : rs2_val;

wire[31:0] inst_addr = {inst_word_addr, 2'b0};

wire inst_is_branch = inst_opcode(inst) == `OPCODE_BRANCH;

/*
 The sign bit means that the offset is negative. Branches backwards are expected to be taken.
 This matches the prediction by the fetcher block, needed once the branch instruction gets to this
  point of the pipeline.
*/
wire branch_expected = inst_is_branch && inst[31];
wire do_branch = inst_is_branch && branch_condition_met(alu_x, alu_y, inst_funct3(inst));
wire branch_cancel = branch_expected && !do_branch;

always @(posedge clk) begin
    if (reset || !hold) begin
        inst_out <= `INST_NOP;
        invalid_op <= 0;
        branch_refetch <= 0;
    end

    if (!reset && !hold && !branch_refetch) begin
        inst_out <= inst;
        ebreak <= inst == `INST_EBREAK;

        case (inst_opcode(inst))
            `OPCODE_OP, `OPCODE_OP_IMM: begin
                write_val <= alu_op(alu_x, alu_y, inst_funct3(inst), inst_funct7(inst),
                    alu_uses_immediate);
            end
            `OPCODE_LOAD: mem_addr <= inst_Iimm(inst) + alu_x;
            `OPCODE_STORE: begin
                write_val <= alu_y;
                mem_addr <= inst_Simm(inst) + alu_x;
            end
            `OPCODE_LUI: write_val <= inst_Uimm(inst);
            `OPCODE_AUIPC: write_val <= inst_Uimm(inst) + inst_addr;
            `OPCODE_BRANCH: begin
                branch_refetch_addr <= branch_cancel ? inst_addr + 4 : inst_Bimm(inst) + inst_addr;
                branch_refetch <= do_branch ^ branch_expected;
            end
            `OPCODE_JAL, `OPCODE_JALR: begin
                // bit 3 of the instruction word is set for JAL and not for JALR.
                branch_refetch_addr <= inst[3] ? inst_Jimm(inst) + inst_addr : alu_x + inst_Iimm(inst);
                write_val <= inst_addr + 4;
                branch_refetch <= 1;
            end
            `OPCODE_SYSTEM: ;
            default: invalid_op <= 1;
        endcase
    end
end

endmodule
