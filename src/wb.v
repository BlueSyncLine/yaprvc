`include "inst.v"
`include "opcodes.v"

module wb(
    input wire hold,
    input wire reset,
    input wire[31:0] inst,
    input wire[31:0] data_in_mem,
    input wire[31:0] data_in_reg,
    input wire[1:0] offset,
    output wire[31:0] data_out,
    output wire[4:0] rd
);

function signed[31:0] sext8;
    input signed[7:0] x;
    sext8 = 32'(x);
endfunction

function signed[31:0] sext16;
    input signed[15:0] x;
    sext16 = 32'(x);
endfunction

function[31:0] load_select;
    input[31:0] value;
    input[1:0] byte_offset;
    input[2:0] funct3;

    load_select = funct3 == `FUNCT3_LB ? sext8(value[byte_offset*8 +: 8]) :
                  funct3 == `FUNCT3_LH ? sext16(value[byte_offset*8 +: 16]) :
                  funct3 == `FUNCT3_LW ? value :
                  funct3 == `FUNCT3_LBU ? {24'b0, value[byte_offset*8 +: 8]} :
                  funct3 == `FUNCT3_LHU ? {16'b0, value[byte_offset*8 +: 16]} :
                  0;
endfunction

assign rd = !reset && !hold && opcode_uses_rd(inst_opcode(inst)) ? inst_rd(inst) : 5'b0;
assign data_out = inst_opcode(inst) == `OPCODE_LOAD ?
    load_select(data_in_mem, offset, inst_funct3(inst)) :
    data_in_reg;

endmodule
