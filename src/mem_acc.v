`include "inst.v"
`include "opcodes.v"
module mem_acc(
    input wire clk,
    input wire hold,
    input wire reset,

    input wire[31:0] inst,
    input wire[31:0] mem_addr,
    input wire[31:0] write_val,
    output reg read,
    output reg write,
    output reg[29:0] access_word_addr,
    output reg[3:0] access_write_mask,
    output reg[31:0] access_write_data,
    output reg[1:0] access_offset,
    output reg alignment_error,
    output wire[31:0] inst_out,
    output wire[31:0] reg_wb_out
);

wire[1:0] access_size = {{inst_funct3(inst)}[1:0]};
wire[1:0] byte_offset = mem_addr[1:0];
wire is_load = inst_opcode(inst) == `OPCODE_LOAD;
wire is_store = inst_opcode(inst) == `OPCODE_STORE;

always @(posedge clk) begin
    if (reset) begin
        inst_out <= `INST_NOP;
        read <= 0;
        write <= 0;
    end

    if (!reset && !hold) begin
        read <= 0;
        write <= 0;
        inst_out <= inst;
        access_word_addr <= mem_addr[31:2];
        reg_wb_out <= write_val;

        read <= is_load;
        write <= is_store;

        // Alignment error: half-word access with an odd address or a word access with the address
        //  not being a multiple of four.
        alignment_error <= (is_load || is_store) && ((access_size == 2'b01 && byte_offset[0]) ||
                                                     (access_size == 2'b10 && |byte_offset));


        case (access_size)
            2'b00: access_write_mask <= 4'b0001 << byte_offset;
            2'b01: access_write_mask <= 4'b0011 << byte_offset;
            2'b10: access_write_mask <= 4'b1111;
            default: ;
        endcase

        access_write_data <= write_val << (byte_offset * 8);
        access_offset <= byte_offset;
    end
end

endmodule
