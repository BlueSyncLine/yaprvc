`include "inst.v"
`include "opcodes.v"

`include "interdepend.v"

module decoder(
    input wire clk,
    input wire hold,
    input wire reset,

    input wire branch_refetch,

    input wire[31:0] inst,
    input wire[29:0] inst_word_addr,

    input wire[31:0] mem_inst,

    output reg[31:0] inst_out,
    output reg[29:0] inst_word_addr_out,

    output wire bubble,
    output reg[1:0] interdepend_exec,

    output reg[4:0] rs1,
    output reg[4:0] rs2,

    output wire branch_expect,
    output wire[31:0] branch_expect_pc
);

// The output of this stage is what the execute stage's input was on the current clock cycle.
wire[31:0] exec_inst = inst_out;
wire interdepend_mem = inst_mem_interdepend(inst, exec_inst) ||
                       inst_mem_interdepend(inst, mem_inst);
assign bubble = interdepend_mem;

// Simple branch prediction as described in the architecture specification.
// Backward branches are assumed to be taken, forward branches are assumed not taken.
// The instruction's MSB is the sign of the immediate, set for backward branches.
assign branch_expect = inst_opcode(inst) == `OPCODE_BRANCH && inst[31];
assign branch_expect_pc = {inst_word_addr, 2'b0} + inst_Bimm(inst);

/*
    A - instruction writing rx
    B - instruction reading rx

    c 0 1 2
    F B
    D A B
    X   A
    M     B
    W     A

    This unit is relevant on cycle 1 of this relative diagram.
    `inst_out` is the previous instruction passed through this stage, which will be
      presented at the input of the execute stage.
    So, when `inst` is B, `inst_out` is A.
    The `interdepend_exec` signals indicate this for rs1 and rs2, so that the execute stage
     will know that instead of a register file access, the previous ALU result should be used
     in the place of a corresponding register value.

    Two more scenarios, where ? is an unrelated instruction:

    c 0 1 2 3
    F ? B
    D A ? B
    X   A ? B
    M     A ?
    W       A

    c 0 1 2 3 4 5 6 7
    F A ? ? B
    D   A ? ? B
    X     A ? ? B
    M       A ? ? B
    W         A ? ? B

*/

always @(posedge clk) begin
    if (reset) begin
        inst_out <= `INST_NOP;
    end

    if (!reset && !hold) begin
        if (!bubble && !branch_refetch) begin
            inst_out <= inst;
            inst_word_addr_out <= inst_word_addr;
            interdepend_exec <= inst_reg_interdepend(inst, exec_inst);

            rs1 <= inst_rs1(inst);
            rs2 <= inst_rs2(inst);
        end else begin
            inst_out <= `INST_NOP;
        end
    end
end

endmodule
