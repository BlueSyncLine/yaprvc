module fetcher(
    input wire clk,
    input wire hold,
    input wire reset,

    input wire branch_expect,
    input wire[31:0] branch_expect_pc,

    input wire branch_refetch,
    input wire bubble,

    input wire[31:0] branch_refetch_addr,
    output reg[29:0] fetch_word_addr,
    output reg inst_alignment_error
);

// Primitive branch prediction is implemented as described in the RISC-V spec.
// Branches back (negative offset) are assumed taken, branches forward not.
// Prefetch according to that rule and stall only when the branch is unexpected.

reg[31:0] pc;

always @(posedge clk) begin
    if (reset) begin
        pc <= 0;
        fetch_word_addr <= 0;
    end

    if (!reset && !hold && !bubble) begin
        if (branch_refetch) begin
            pc <= branch_refetch_addr + 4;
            fetch_word_addr <= branch_refetch_addr[31:2];
        end else if (branch_expect) begin
            pc <= branch_expect_pc + 4;
            fetch_word_addr <= branch_expect_pc[31:2];
        end else begin
            pc <= pc + 4;
            fetch_word_addr <= pc[31:2];
        end
    end

    inst_alignment_error <= |pc[1:0];
end

endmodule
