module regfile(
    input wire clk,
    input wire hold,
    input wire[4:0] rs1,
    input wire[4:0] rs2,
    input wire[4:0] rd,
    input wire[31:0] write_data,
    output wire[31:0] read_data_1,
    output wire[31:0] read_data_2
);

reg[31:0] reg_array[30:0];

/*
    Register read access macro with special handling for the source and destination reg being
     the same.
*/
`define regs_read(rs) (rs == 5'b0 ? 32'b0 : rs == rd ? write_data : reg_array[rs - 1])

assign read_data_1 = `regs_read(rs1);
assign read_data_2 = `regs_read(rs2);

always @(posedge clk) begin
    if (!hold && rd != 5'b0) begin
        reg_array[rd - 1] <= write_data;
    end
end

endmodule
