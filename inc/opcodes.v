`ifndef OPCODES_V
`define OPCODES_V
`define OPCODE_LOAD     7'b00_000_11
`define OPCODE_MISC_MEM 7'b00_011_11
`define OPCODE_OP_IMM   7'b00_100_11
`define OPCODE_AUIPC    7'b00_101_11
`define OPCODE_STORE    7'b01_000_11
`define OPCODE_OP       7'b01_100_11
`define OPCODE_LUI      7'b01_101_11
`define OPCODE_BRANCH   7'b11_000_11
`define OPCODE_JALR     7'b11_001_11
`define OPCODE_JAL      7'b11_011_11
`define OPCODE_SYSTEM   7'b11_100_11

`define FUNCT3_LB    3'b000
`define FUNCT3_LH    3'b001
`define FUNCT3_LW    3'b010
`define FUNCT3_LBU   3'b100
`define FUNCT3_LHU   3'b101

function opcode_uses_rd;
    input[6:0] opcode;
    opcode_uses_rd = opcode == `OPCODE_LOAD ||
                     opcode == `OPCODE_OP || opcode == `OPCODE_OP_IMM ||
                     opcode == `OPCODE_LUI || opcode == `OPCODE_AUIPC ||
                     opcode == `OPCODE_JAL || opcode == `OPCODE_JALR ||
                     opcode == `OPCODE_MISC_MEM;
endfunction

function opcode_uses_rs1;
    input[6:0] opcode;
    opcode_uses_rs1 = opcode == `OPCODE_LOAD || opcode == `OPCODE_STORE ||
                      opcode == `OPCODE_OP || opcode == `OPCODE_OP_IMM ||
                      opcode == `OPCODE_BRANCH || opcode == `OPCODE_JALR ||
                      opcode == `OPCODE_MISC_MEM;
endfunction

function opcode_uses_rs2;
    input[6:0] opcode;
    opcode_uses_rs2 = opcode == `OPCODE_STORE || opcode == `OPCODE_OP ||
                      opcode == `OPCODE_BRANCH;
endfunction

function opcode_is_load;
    input[6:0] opcode;
    opcode_is_load = opcode == `OPCODE_LOAD;
endfunction
`endif
