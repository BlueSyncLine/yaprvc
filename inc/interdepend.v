`ifndef INTERDEPEND_V
`define INTERDEPEND_V
`include "inst.v"
`include "opcodes.v"

function[4:0] inst_inter_dest_reg;
    input[31:0] inst;
    inst_inter_dest_reg = opcode_uses_rd(inst_opcode(inst)) ? inst_rd(inst) : 5'b0;
endfunction

function[4:0] inst_inter_src_reg_1;
    input[31:0] inst;
    inst_inter_src_reg_1 = opcode_uses_rs1(inst_opcode(inst)) ? inst_rs1(inst) : 5'b0;
endfunction

function[4:0] inst_inter_src_reg_2;
    input[31:0] inst;
    inst_inter_src_reg_2 = opcode_uses_rs2(inst_opcode(inst)) ? inst_rs2(inst) : 5'b0;
endfunction

function reg_reg_interdepend;
    input[4:0] src;
    input[4:0] dest;

    reg_reg_interdepend = src != 5'b0 && dest == src;
endfunction

function[1:0] inst_reg_interdepend;
    input[31:0] cur_inst;
    input[31:0] prev_inst;
    inst_reg_interdepend = {
        reg_reg_interdepend(inst_inter_src_reg_2(cur_inst), inst_inter_dest_reg(prev_inst)),
        reg_reg_interdepend(inst_inter_src_reg_1(cur_inst), inst_inter_dest_reg(prev_inst))
    };
endfunction

function inst_is_load;
    input[31:0] inst;
    inst_is_load = opcode_is_load(inst_opcode(inst));
endfunction

function inst_mem_interdepend;
    input[31:0] cur_inst;
    input[31:0] mem_inst;
    inst_mem_interdepend = inst_is_load(mem_inst) && |inst_reg_interdepend(cur_inst, mem_inst);
endfunction;
`endif
