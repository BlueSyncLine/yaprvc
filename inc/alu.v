`ifndef ALU_V
`define ALU_V
function signed_lt;
    input signed[31:0] x;
    input signed[31:0] y;

    signed_lt = x < y;
endfunction

// It seems really difficult to convince the compiler that the operand is signed...
function[31:0] signed_asr;
    input signed[31:0] x;
    input[4:0] shamt;

    signed_asr = x >>> shamt;
endfunction

function branch_condition_met;
    input[31:0] x;
    input[31:0] y;
    input[2:0] funct3;

    /*
        A rule can be observed here: the lowest bit of funct3 corresponds to the inversion of the
         branch condition specified by two upper bits.
    */
    branch_condition_met = funct3[0] ^ (
        funct3[2:1] == 2'b00 ? x == y :
        funct3[2:1] == 2'b10 ? signed_lt(x, y) :
        funct3[2:1] == 2'b11 ? x < y : 0);
endfunction

/* verilator lint_off UNUSEDSIGNAL */
function[31:0] alu_op;
    input[31:0] x;
    input[31:0] y;
    input[2:0] funct3;
    input[6:0] funct7;
    input use_immediate;

    alu_op = funct3 == 3'b000 ? (!use_immediate && funct7[5] ? x - y : x + y) :
             funct3 == 3'b001 ? x << y[4:0] :
             funct3 == 3'b010 ? {31'b0, signed_lt(x, y)} :
             funct3 == 3'b011 ? {31'b0, x < y} :
             funct3 == 3'b100 ? x ^ y :
             funct3 == 3'b101 ? (funct7[5] ? signed_asr(x, y[4:0]) : x >> y[4:0]) :
             funct3 == 3'b110 ? x | y :
             funct3 == 3'b111 ? x & y : 0;
endfunction
/* verilator lint_on UNUSEDSIGNAL */
`endif
