`ifndef INST_V
`define INST_V

/* verilator lint_off UNUSEDSIGNAL */

function[6:0] inst_funct7;
    input[31:0] inst;
    inst_funct7 = inst[31:25];
endfunction

function[4:0] inst_rs2;
    input[31:0] inst;
    inst_rs2 = inst[24:20];
endfunction

function[4:0] inst_rs1;
    input[31:0] inst;
    inst_rs1 = inst[19:15];
endfunction

function[2:0] inst_funct3;
    input[31:0] inst;
    inst_funct3 = inst[14:12];
endfunction

function[4:0] inst_rd;
    input[31:0] inst;
    inst_rd = inst[11:7];
endfunction

function[6:0] inst_opcode;
    input[31:0] inst;
    inst_opcode = inst[6:0];
endfunction

function[31:0] inst_Iimm;
    input[31:0] inst;
    inst_Iimm = {{20{inst[31]}}, inst[31:20]};
endfunction

function[31:0] inst_Simm;
    input[31:0] inst;
    inst_Simm = {{20{inst[31]}}, inst[31:25], inst[11:7]};
endfunction

function[31:0] inst_Bimm;
    input[31:0] inst;
    inst_Bimm = {{20{inst[31]}}, inst[7], inst[30:25], inst[11:8], 1'b0};
endfunction

function[31:0] inst_Uimm;
    input[31:0] inst;
    inst_Uimm = {inst[31:12], 12'b0};
endfunction

function[31:0] inst_Jimm;
    input[31:0] inst;
    inst_Jimm = {{12{inst[31]}}, inst[19:12], inst[20], inst[30:21], 1'b0};
endfunction

/* verilator lint_on UNUSEDSIGNAL */

// addi x0, x0, 0
`define INST_NOP    32'b000000000000_00000_000_00000_0010011
`define INST_EBREAK 32'b000000000001_00000_000_00000_1110011
`endif
