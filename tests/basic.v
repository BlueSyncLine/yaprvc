module testbench;

reg clk = 0;
always #1
    clk = !clk;

reg[31:0] mem[16383:0];
wire[29:0] inst_word_addr, data_word_addr;
reg[31:0] inst_word;
reg[31:0] data_word_read;
wire[31:0] data_word_write;
wire[3:0] data_write_mask;
wire data_read, data_write, breakpoint;
reg reset;

/* verilator lint_off PINMISSING */
core uut(.clk(clk), .reset(reset), .inst_word_addr(inst_word_addr), .inst_word(inst_word),
    .data_word_addr(data_word_addr), .data_word_read(data_word_read), .data_word_write(data_word_write),
    .data_read(data_read), .data_write(data_write), .breakpoint(breakpoint), .data_write_mask(data_write_mask));
/* verilator lint_on PINMISSING */

wire[31:0] write_mask_ext = {
    {8{data_write_mask[3]}},
    {8{data_write_mask[2]}},
    {8{data_write_mask[1]}},
    {8{data_write_mask[0]}}
};

always @(negedge clk) begin
    data_word_read = mem[data_word_addr[13:0]];

    if (data_write) begin
        if (data_word_addr == 30'h4000000) begin
            $write("%c", data_word_write[7:0]);
        end else begin
            mem[data_word_addr[13:0]] = (data_word_write & write_mask_ext) |
                                         (data_word_read & ~write_mask_ext);
        end
    end

    inst_word = mem[inst_word_addr[13:0]];

    if (breakpoint) begin
        $display("Test program failed!");
        $fatal;
    end
end

initial begin
    $dumpfile("basic.vcd");
    $dumpvars(0, testbench);
    $readmemh("rvtests/firmware.hex", mem);

    reset = 1;
    #2
    reset = 0;

    #50000
    $finish;
end

endmodule
