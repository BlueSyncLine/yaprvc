# Requires the binaries from oss-cad-suite to be in PATH.

all: lint sim

lint: src/*.v
	verilator --lint-only -Wall -Iinc $^

sim: src/*.v tests/basic.v
	verilator -j 0 --binary --trace --top-module testbench -Iinc $^
	obj_dir/Vtestbench

